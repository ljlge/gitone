package cn.t102.orderdishessystem.service;

import cn.t102.orderdishessystem.pojo.OdsUser;
import cn.t102.orderdishessystem.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    //登录
    public User getUserByUserName(String userName,
                                    String password  );
    //用户查询所有
    public List<User> getUserList();

    //根据姓名模糊查询
    public List<OdsUser> getUserListByName(String userName);

    //添加
    public int addUser(OdsUser user);

    //修改
    public int updateUser(OdsUser user);

    public int deleteBy(OdsUser odsUser);
}
