package cn.t102.orderdishessystem.service;


import cn.t102.orderdishessystem.dao.OdsUserMapper;
import cn.t102.orderdishessystem.dao.UserMapper;
import cn.t102.orderdishessystem.pojo.OdsUser;
import cn.t102.orderdishessystem.pojo.OdsUserExample;
import cn.t102.orderdishessystem.pojo.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServcieImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Resource
    private OdsUserMapper odsUserMapper;

    @Override
    public User getUserByUserName(String userName, String password) {

        return userMapper.getUserByUserName(userName, password);
    }

    @Override
    public List<User> getUserList() {
        return userMapper.getUserList();
    }

    @Override
    public List<OdsUser> getUserListByName(String userName) {
        //1.封装example作为使用
        OdsUserExample example = new OdsUserExample();
        OdsUserExample.Criteria criteria = example.createCriteria();
        criteria.andUsernameLike(userName);
        //2.调用mapper接口中的
        List<OdsUser> odsUsers = odsUserMapper.selectByExample(example);
        return odsUsers;
    }

    @Override
    public int addUser(OdsUser user) {

        return odsUserMapper.insert(user);
    }

    @Override
    public int updateUser(OdsUser user) {
        //   odsUserMapper
        return 0;
    }

    @Override
    public int deleteBy(OdsUser odsUser) {

         return odsUserMapper.deleteByExample(odsUser.getId());

    }

}
