package cn.t102.orderdishessystem.dao;

import cn.t102.orderdishessystem.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    //登录
    public User getUserByUserName(@Param("userName")String userName,
                                  @Param("password")  String password  );

    //查询所有用户
    public List<User> getUserList();
}
