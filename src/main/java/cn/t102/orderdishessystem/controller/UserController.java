package cn.t102.orderdishessystem.controller;

import cn.t102.orderdishessystem.pojo.User;
import cn.t102.orderdishessystem.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class UserController {
    @Resource
    private UserService userService;

    @RequestMapping("/login")
    public String  login(@RequestParam("userName") String userName,
                         @RequestParam("password") String password,
                         Model model)
    {
        User userByUserName = userService.getUserByUserName(userName, password);
        if (userByUserName==null)
        {
            model.addAttribute("errorInfo","用户名或者密码不正确！");
            return "login";
        }
        model.addAttribute("userByUserName",userByUserName);
        return "index";
    }

    @RequestMapping("view/userList.html")
    public String list(Model model,@RequestParam(value = "pageIndex",defaultValue = "1",required = false)Integer pageIndex){

        //设置分页规则： 第一个参数：页码，第二个参数：每页大小
        PageHelper.startPage(pageIndex,3);

        //封装PageInfo
        PageInfo<User> pageInfo = new PageInfo<User>(userService.getUserList());
        System.out.println(pageInfo);
        List<User> userList = userService.getUserList();
        model.addAttribute("pageInfo",pageInfo);
        return "view/userList";
    }
}
