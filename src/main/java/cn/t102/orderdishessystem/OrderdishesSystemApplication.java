package cn.t102.orderdishessystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages =("cn.t102"))
public class OrderdishesSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderdishesSystemApplication.class, args);
    }
}
