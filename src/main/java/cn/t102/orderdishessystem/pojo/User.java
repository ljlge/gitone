package cn.t102.orderdishessystem.pojo;

import java.util.Date;

public class User {
    private String id;
    private String userCode;
    private String userName;
    private String phone;
    private Date registerTime;
    private int loginCount;
    private int score;
    private Date lastLoginTime;
    private String ip;
    private String password;

    public User(String id, String userCode, String userName, String phone, Date registerTime, int loginCount, int score, Date lastLoginTime, String ip, String password) {
        this.id = id;
        this.userCode = userCode;
        this.userName = userName;
        this.phone = phone;
        this.registerTime = registerTime;
        this.loginCount = loginCount;
        this.score = score;
        this.lastLoginTime = lastLoginTime;
        this.ip = ip;
        this.password = password;
    }

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", userCode='" + userCode + '\'' +
                ", userName='" + userName + '\'' +
                ", phone='" + phone + '\'' +
                ", registerTime=" + registerTime +
                ", loginCount=" + loginCount +
                ", score=" + score +
                ", lastLoginTime=" + lastLoginTime +
                ", ip='" + ip + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public int getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(int loginCount) {
        this.loginCount = loginCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
