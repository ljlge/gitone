package cn.t102.orderdishessystem;

import cn.t102.orderdishessystem.pojo.OdsUser;
import cn.t102.orderdishessystem.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class OrderdishesSystemApplicationTests {

    @Resource
    private UserService userService;
    @Test
    void contextLoads() {
        List<OdsUser> userListByName = userService.getUserListByName("%张%");
        for (OdsUser odsUser : userListByName) {
            System.out.println("====" + odsUser);
        }
    }
    @Test
    void insert(){
        OdsUser odsUser = new OdsUser();
        odsUser.setUsername("牛");
        odsUser.setUsercode("niu");
        odsUser.setIp("1234");
        odsUser.setPassword("111");
         userService.addUser(odsUser);
    }

}
